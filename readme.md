# Electronics eCommerce Website

### Tools:
Vs Code
### Language: 
Html,Css,Laravel,Php
 

 **Description:**
    
    In this project,there is User-End and Admin-End.
	
 **User-End:**

 * This view contains the home page menu bar and image slider.

* ![Front Top view](https://bitbucket.org/aliahmad217/electronics-ecommerce/raw/7299e70d1ca5975cac5df58ddc9635d717988cc3/images/Screenshot_2.png)

* This view contains the home page wit products list.

* ![Front Top view](https://bitbucket.org/aliahmad217/electronics-ecommerce/raw/7299e70d1ca5975cac5df58ddc9635d717988cc3/images/Screenshot_12.png)

* This view contains the product detail.

* ![Front Top view](https://bitbucket.org/aliahmad217/electronics-ecommerce/raw/7299e70d1ca5975cac5df58ddc9635d717988cc3/images/Screenshot_13.png)

* This view contains the product that is added to cart.

* ![Front Top view](https://bitbucket.org/aliahmad217/electronics-ecommerce/raw/7299e70d1ca5975cac5df58ddc9635d717988cc3/images/Screenshot_14.png)

* This view contains the sign in and signup form for user.

* ![Front Top view](https://bitbucket.org/aliahmad217/electronics-ecommerce/raw/7299e70d1ca5975cac5df58ddc9635d717988cc3/images/Screenshot_15.png)

* This view contains the required form for shipment details.

* ![Front Top view](https://bitbucket.org/aliahmad217/electronics-ecommerce/raw/7299e70d1ca5975cac5df58ddc9635d717988cc3/images/Screenshot_16.png)

* This view contains the selection payment method of user choice.

* ![Front Top view](https://bitbucket.org/aliahmad217/electronics-ecommerce/raw/7299e70d1ca5975cac5df58ddc9635d717988cc3/images/Screenshot_17.png)

* After checkout suucessfully

* ![Front Top view](https://bitbucket.org/aliahmad217/electronics-ecommerce/raw/7299e70d1ca5975cac5df58ddc9635d717988cc3/images/Screenshot_18.png)

## Admin-End:
 Following are the Interface of Admin-End
 
* ![Front Top view](https://bitbucket.org/aliahmad217/electronics-ecommerce/raw/7299e70d1ca5975cac5df58ddc9635d717988cc3/images/Screenshot_4.png)
* ![Front Top view](https://bitbucket.org/aliahmad217/electronics-ecommerce/raw/7299e70d1ca5975cac5df58ddc9635d717988cc3/images/Screenshot_5.png)
* ![Front Top view](https://bitbucket.org/aliahmad217/electronics-ecommerce/raw/7299e70d1ca5975cac5df58ddc9635d717988cc3/images/Screenshot_6.png)
* ![Front Top view](https://bitbucket.org/aliahmad217/electronics-ecommerce/raw/7299e70d1ca5975cac5df58ddc9635d717988cc3/images/Screenshot_7.png)
* ![Front Top view](https://bitbucket.org/aliahmad217/electronics-ecommerce/raw/7299e70d1ca5975cac5df58ddc9635d717988cc3/images/Screenshot_8.png)
* ![Front Top view](https://bitbucket.org/aliahmad217/electronics-ecommerce/raw/7299e70d1ca5975cac5df58ddc9635d717988cc3/images/Screenshot_9.png)
* ![Front Top view](https://bitbucket.org/aliahmad217/electronics-ecommerce/raw/7299e70d1ca5975cac5df58ddc9635d717988cc3/images/Screenshot_10.png)
* ![Front Top view](https://bitbucket.org/aliahmad217/electronics-ecommerce/raw/7299e70d1ca5975cac5df58ddc9635d717988cc3/images/Screenshot_11.png)

